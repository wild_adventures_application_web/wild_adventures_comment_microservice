
CREATE SEQUENCE comment_id_seq;

CREATE TABLE comment (
                id BIGINT NOT NULL DEFAULT nextval('comment_id_seq'),
                item_id BIGINT NOT NULL,
                user_id BIGINT NOT NULL,
                content VARCHAR NOT NULL,
                CONSTRAINT comment_pk PRIMARY KEY (id)
);

ALTER SEQUENCE comment_id_seq OWNED BY comment.id;
