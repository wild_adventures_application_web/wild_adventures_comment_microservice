--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.12
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: comment; Type: TABLE DATA; Schema: public; Owner: adventurer
--

INSERT INTO comment VALUES (4, 74, 1, 'Une super aventure');
INSERT INTO comment VALUES (5, 49, 1, 'Une super aventure');
INSERT INTO comment VALUES (6, 57, 1, 'Une super aventure');
INSERT INTO comment VALUES (7, 22, 1, 'Une super aventure');
INSERT INTO comment VALUES (8, 63, 1, 'Une super aventure');
INSERT INTO comment VALUES (9, 95, 1, 'Une super aventure');
INSERT INTO comment VALUES (10, 1, 1, 'Une super aventure');
INSERT INTO comment VALUES (11, 62, 1, 'Une super aventure');
INSERT INTO comment VALUES (12, 49, 2, 'trop bien');
INSERT INTO comment VALUES (13, 57, 2, 'trop bien');
INSERT INTO comment VALUES (14, 74, 2, 'trop bien');
INSERT INTO comment VALUES (15, 49, 3, 'super top');
INSERT INTO comment VALUES (16, 57, 3, 'super top');
INSERT INTO comment VALUES (17, 74, 3, 'super top');
INSERT INTO comment VALUES (18, 63, 2, 'trop bien');
INSERT INTO comment VALUES (19, 22, 2, 'trop bien');
INSERT INTO comment VALUES (20, 95, 2, 'trop bien');
INSERT INTO comment VALUES (21, 95, 3, 'super top');
INSERT INTO comment VALUES (22, 63, 3, 'super top');
INSERT INTO comment VALUES (23, 62, 2, 'trop bien');
INSERT INTO comment VALUES (24, 22, 3, 'super top');
INSERT INTO comment VALUES (25, 62, 3, 'super top');
INSERT INTO comment VALUES (26, 1, 2, 'trop bien');
INSERT INTO comment VALUES (27, 64, 1, 'Une super aventure');
INSERT INTO comment VALUES (28, 64, 2, 'trop bien');
INSERT INTO comment VALUES (29, 64, 3, 'super top');
INSERT INTO comment VALUES (30, 89, 1, 'Une super aventure');
INSERT INTO comment VALUES (31, 89, 2, 'trop bien');
INSERT INTO comment VALUES (32, 1, 3, 'super top');
INSERT INTO comment VALUES (33, 73, 1, 'Une super aventure');
INSERT INTO comment VALUES (34, 46, 1, 'Une super aventure');
INSERT INTO comment VALUES (35, 94, 1, 'Une super aventure');
INSERT INTO comment VALUES (36, 85, 1, 'Une super aventure');
INSERT INTO comment VALUES (37, 85, 2, 'trop bien');
INSERT INTO comment VALUES (38, 73, 2, 'trop bien');
INSERT INTO comment VALUES (39, 83, 1, 'Une super aventure');
INSERT INTO comment VALUES (40, 94, 2, 'trop bien');
INSERT INTO comment VALUES (41, 94, 3, 'super top');
INSERT INTO comment VALUES (42, 46, 2, 'trop bien');
INSERT INTO comment VALUES (43, 83, 2, 'trop bien');
INSERT INTO comment VALUES (44, 83, 3, 'super top');
INSERT INTO comment VALUES (45, 89, 3, 'super top');
INSERT INTO comment VALUES (46, 73, 3, 'super top');
INSERT INTO comment VALUES (47, 46, 3, 'super top');
INSERT INTO comment VALUES (48, 91, 1, 'Une super aventure');
INSERT INTO comment VALUES (49, 86, 1, 'Une super aventure');
INSERT INTO comment VALUES (50, 91, 2, 'trop bien');
INSERT INTO comment VALUES (51, 81, 1, 'Une super aventure');
INSERT INTO comment VALUES (52, 87, 1, 'Une super aventure');
INSERT INTO comment VALUES (53, 76, 1, 'Une super aventure');
INSERT INTO comment VALUES (54, 87, 2, 'trop bien');
INSERT INTO comment VALUES (55, 76, 2, 'trop bien');
INSERT INTO comment VALUES (56, 76, 3, 'super top');
INSERT INTO comment VALUES (57, 85, 3, 'super top');
INSERT INTO comment VALUES (58, 91, 3, 'super top');
INSERT INTO comment VALUES (59, 86, 2, 'trop bien');
INSERT INTO comment VALUES (60, 81, 2, 'trop bien');
INSERT INTO comment VALUES (61, 20, 1, 'Une super aventure');
INSERT INTO comment VALUES (62, 86, 3, 'super top');
INSERT INTO comment VALUES (63, 81, 3, 'super top');
INSERT INTO comment VALUES (64, 20, 2, 'trop bien');
INSERT INTO comment VALUES (65, 93, 1, 'Une super aventure');
INSERT INTO comment VALUES (66, 93, 2, 'trop bien');
INSERT INTO comment VALUES (67, 20, 3, 'super top');
INSERT INTO comment VALUES (68, 93, 3, 'super top');
INSERT INTO comment VALUES (69, 27, 1, 'Une super aventure');
INSERT INTO comment VALUES (71, 27, 2, 'trop bien');
INSERT INTO comment VALUES (70, 48, 1, 'Une super aventure');
INSERT INTO comment VALUES (72, 87, 3, 'super top');
INSERT INTO comment VALUES (73, 48, 2, 'trop bien');
INSERT INTO comment VALUES (74, 48, 3, 'super top');
INSERT INTO comment VALUES (75, 27, 3, 'super top');
INSERT INTO comment VALUES (76, 35, 1, 'Une super aventure');
INSERT INTO comment VALUES (77, 35, 2, 'trop bien');
INSERT INTO comment VALUES (78, 35, 3, 'super top');
INSERT INTO comment VALUES (79, 37, 1, 'Une super aventure');
INSERT INTO comment VALUES (80, 75, 1, 'Une super aventure');
INSERT INTO comment VALUES (81, 11, 1, 'Une super aventure');
INSERT INTO comment VALUES (82, 75, 2, 'trop bien');
INSERT INTO comment VALUES (83, 15, 1, 'Une super aventure');
INSERT INTO comment VALUES (84, 15, 2, 'trop bien');
INSERT INTO comment VALUES (85, 75, 3, 'super top');
INSERT INTO comment VALUES (86, 12, 1, 'Une super aventure');
INSERT INTO comment VALUES (87, 12, 2, 'trop bien');
INSERT INTO comment VALUES (88, 15, 3, 'super top');
INSERT INTO comment VALUES (89, 12, 3, 'super top');
INSERT INTO comment VALUES (90, 11, 2, 'trop bien');
INSERT INTO comment VALUES (91, 37, 2, 'trop bien');
INSERT INTO comment VALUES (92, 37, 3, 'super top');
INSERT INTO comment VALUES (93, 18, 1, 'Une super aventure');
INSERT INTO comment VALUES (94, 18, 2, 'trop bien');
INSERT INTO comment VALUES (95, 11, 3, 'super top');
INSERT INTO comment VALUES (96, 18, 3, 'super top');
INSERT INTO comment VALUES (97, 40, 1, 'Une super aventure');
INSERT INTO comment VALUES (98, 40, 2, 'trop bien');
INSERT INTO comment VALUES (99, 40, 3, 'super top');
INSERT INTO comment VALUES (100, 79, 1, 'Une super aventure');
INSERT INTO comment VALUES (101, 59, 1, 'Une super aventure');
INSERT INTO comment VALUES (102, 59, 2, 'trop bien');
INSERT INTO comment VALUES (103, 79, 2, 'trop bien');
INSERT INTO comment VALUES (104, 79, 3, 'super top');
INSERT INTO comment VALUES (105, 59, 3, 'super top');
INSERT INTO comment VALUES (106, 29, 1, 'Une super aventure');
INSERT INTO comment VALUES (107, 26, 1, 'Une super aventure');
INSERT INTO comment VALUES (108, 29, 2, 'trop bien');
INSERT INTO comment VALUES (109, 50, 1, 'Une super aventure');
INSERT INTO comment VALUES (110, 50, 2, 'trop bien');
INSERT INTO comment VALUES (111, 29, 3, 'super top');
INSERT INTO comment VALUES (112, 26, 2, 'trop bien');
INSERT INTO comment VALUES (113, 5, 1, 'Une super aventure');
INSERT INTO comment VALUES (114, 50, 3, 'super top');
INSERT INTO comment VALUES (116, 26, 3, 'super top');
INSERT INTO comment VALUES (121, 10, 1, 'Une super aventure');
INSERT INTO comment VALUES (125, 10, 2, 'trop bien');
INSERT INTO comment VALUES (134, 10, 3, 'super top');
INSERT INTO comment VALUES (151, 24, 1, 'Une super aventure');
INSERT INTO comment VALUES (153, 24, 2, 'trop bien');
INSERT INTO comment VALUES (154, 24, 3, 'super top');
INSERT INTO comment VALUES (180, 0, 1, 'Une super aventure');
INSERT INTO comment VALUES (199, 0, 2, 'trop bien');
INSERT INTO comment VALUES (200, 0, 3, 'super top');
INSERT INTO comment VALUES (219, 88, 1, 'Une super aventure');
INSERT INTO comment VALUES (221, 88, 2, 'trop bien');
INSERT INTO comment VALUES (223, 88, 3, 'super top');
INSERT INTO comment VALUES (247, 71, 1, 'Une super aventure');
INSERT INTO comment VALUES (252, 71, 2, 'trop bien');
INSERT INTO comment VALUES (254, 71, 3, 'super top');
INSERT INTO comment VALUES (276, 51, 1, 'Une super aventure');
INSERT INTO comment VALUES (277, 51, 2, 'trop bien');
INSERT INTO comment VALUES (279, 51, 3, 'super top');
INSERT INTO comment VALUES (117, 23, 1, 'Une super aventure');
INSERT INTO comment VALUES (126, 23, 2, 'trop bien');
INSERT INTO comment VALUES (133, 23, 3, 'super top');
INSERT INTO comment VALUES (158, 2, 1, 'Une super aventure');
INSERT INTO comment VALUES (163, 2, 2, 'trop bien');
INSERT INTO comment VALUES (172, 2, 3, 'super top');
INSERT INTO comment VALUES (181, 92, 1, 'Une super aventure');
INSERT INTO comment VALUES (183, 92, 2, 'trop bien');
INSERT INTO comment VALUES (184, 92, 3, 'super top');
INSERT INTO comment VALUES (206, 69, 1, 'Une super aventure');
INSERT INTO comment VALUES (208, 69, 2, 'trop bien');
INSERT INTO comment VALUES (210, 69, 3, 'super top');
INSERT INTO comment VALUES (226, 3, 1, 'Une super aventure');
INSERT INTO comment VALUES (228, 3, 2, 'trop bien');
INSERT INTO comment VALUES (234, 3, 3, 'super top');
INSERT INTO comment VALUES (244, 68, 1, 'Une super aventure');
INSERT INTO comment VALUES (262, 68, 2, 'trop bien');
INSERT INTO comment VALUES (269, 68, 3, 'super top');
INSERT INTO comment VALUES (118, 5, 2, 'trop bien');
INSERT INTO comment VALUES (131, 5, 3, 'super top');
INSERT INTO comment VALUES (155, 34, 1, 'Une super aventure');
INSERT INTO comment VALUES (166, 34, 2, 'trop bien');
INSERT INTO comment VALUES (168, 34, 3, 'super top');
INSERT INTO comment VALUES (185, 17, 1, 'Une super aventure');
INSERT INTO comment VALUES (191, 17, 2, 'trop bien');
INSERT INTO comment VALUES (201, 17, 3, 'super top');
INSERT INTO comment VALUES (218, 65, 1, 'Une super aventure');
INSERT INTO comment VALUES (224, 65, 2, 'trop bien');
INSERT INTO comment VALUES (236, 65, 3, 'super top');
INSERT INTO comment VALUES (258, 7, 1, 'Une super aventure');
INSERT INTO comment VALUES (265, 7, 2, 'trop bien');
INSERT INTO comment VALUES (270, 7, 3, 'super top');
INSERT INTO comment VALUES (119, 25, 1, 'Une super aventure');
INSERT INTO comment VALUES (128, 25, 2, 'trop bien');
INSERT INTO comment VALUES (136, 25, 3, 'super top');
INSERT INTO comment VALUES (146, 6, 1, 'Une super aventure');
INSERT INTO comment VALUES (150, 6, 2, 'trop bien');
INSERT INTO comment VALUES (165, 6, 3, 'super top');
INSERT INTO comment VALUES (182, 43, 1, 'Une super aventure');
INSERT INTO comment VALUES (187, 43, 2, 'trop bien');
INSERT INTO comment VALUES (190, 43, 3, 'super top');
INSERT INTO comment VALUES (222, 4, 1, 'Une super aventure');
INSERT INTO comment VALUES (229, 4, 2, 'trop bien');
INSERT INTO comment VALUES (235, 4, 3, 'super top');
INSERT INTO comment VALUES (259, 60, 1, 'Une super aventure');
INSERT INTO comment VALUES (264, 60, 2, 'trop bien');
INSERT INTO comment VALUES (266, 60, 3, 'super top');
INSERT INTO comment VALUES (120, 45, 1, 'Une super aventure');
INSERT INTO comment VALUES (127, 45, 2, 'trop bien');
INSERT INTO comment VALUES (130, 45, 3, 'super top');
INSERT INTO comment VALUES (145, 80, 1, 'Une super aventure');
INSERT INTO comment VALUES (147, 80, 2, 'trop bien');
INSERT INTO comment VALUES (161, 80, 3, 'super top');
INSERT INTO comment VALUES (175, 77, 1, 'Une super aventure');
INSERT INTO comment VALUES (176, 77, 2, 'trop bien');
INSERT INTO comment VALUES (178, 77, 3, 'super top');
INSERT INTO comment VALUES (202, 53, 1, 'Une super aventure');
INSERT INTO comment VALUES (203, 53, 2, 'trop bien');
INSERT INTO comment VALUES (204, 53, 3, 'super top');
INSERT INTO comment VALUES (220, 28, 1, 'Une super aventure');
INSERT INTO comment VALUES (231, 28, 2, 'trop bien');
INSERT INTO comment VALUES (237, 28, 3, 'super top');
INSERT INTO comment VALUES (246, 32, 1, 'Une super aventure');
INSERT INTO comment VALUES (248, 32, 2, 'trop bien');
INSERT INTO comment VALUES (251, 32, 3, 'super top');
INSERT INTO comment VALUES (274, 61, 1, 'Une super aventure');
INSERT INTO comment VALUES (278, 61, 2, 'trop bien');
INSERT INTO comment VALUES (282, 61, 3, 'super top');
INSERT INTO comment VALUES (122, 44, 1, 'Une super aventure');
INSERT INTO comment VALUES (132, 44, 2, 'trop bien');
INSERT INTO comment VALUES (137, 44, 3, 'super top');
INSERT INTO comment VALUES (160, 9, 1, 'Une super aventure');
INSERT INTO comment VALUES (164, 9, 2, 'trop bien');
INSERT INTO comment VALUES (169, 9, 3, 'super top');
INSERT INTO comment VALUES (188, 8, 1, 'Une super aventure');
INSERT INTO comment VALUES (192, 8, 2, 'trop bien');
INSERT INTO comment VALUES (193, 8, 3, 'super top');
INSERT INTO comment VALUES (239, 39, 1, 'Une super aventure');
INSERT INTO comment VALUES (241, 39, 2, 'trop bien');
INSERT INTO comment VALUES (243, 39, 3, 'super top');
INSERT INTO comment VALUES (271, 67, 1, 'Une super aventure');
INSERT INTO comment VALUES (272, 67, 2, 'trop bien');
INSERT INTO comment VALUES (273, 67, 3, 'super top');
INSERT INTO comment VALUES (123, 42, 1, 'Une super aventure');
INSERT INTO comment VALUES (129, 42, 2, 'trop bien');
INSERT INTO comment VALUES (139, 42, 3, 'super top');
INSERT INTO comment VALUES (152, 38, 1, 'Une super aventure');
INSERT INTO comment VALUES (156, 38, 2, 'trop bien');
INSERT INTO comment VALUES (159, 38, 3, 'super top');
INSERT INTO comment VALUES (177, 78, 1, 'Une super aventure');
INSERT INTO comment VALUES (179, 78, 2, 'trop bien');
INSERT INTO comment VALUES (198, 78, 3, 'super top');
INSERT INTO comment VALUES (214, 70, 1, 'Une super aventure');
INSERT INTO comment VALUES (225, 70, 2, 'trop bien');
INSERT INTO comment VALUES (227, 70, 3, 'super top');
INSERT INTO comment VALUES (249, 41, 1, 'Une super aventure');
INSERT INTO comment VALUES (256, 41, 2, 'trop bien');
INSERT INTO comment VALUES (267, 41, 3, 'super top');
INSERT INTO comment VALUES (283, 54, 1, 'Une super aventure');
INSERT INTO comment VALUES (285, 54, 2, 'trop bien');
INSERT INTO comment VALUES (287, 54, 3, 'super top');
INSERT INTO comment VALUES (124, 82, 1, 'Une super aventure');
INSERT INTO comment VALUES (138, 82, 2, 'trop bien');
INSERT INTO comment VALUES (141, 82, 3, 'super top');
INSERT INTO comment VALUES (148, 21, 1, 'Une super aventure');
INSERT INTO comment VALUES (149, 21, 2, 'trop bien');
INSERT INTO comment VALUES (170, 21, 3, 'super top');
INSERT INTO comment VALUES (186, 55, 1, 'Une super aventure');
INSERT INTO comment VALUES (189, 55, 2, 'trop bien');
INSERT INTO comment VALUES (197, 55, 3, 'super top');
INSERT INTO comment VALUES (230, 58, 1, 'Une super aventure');
INSERT INTO comment VALUES (232, 58, 2, 'trop bien');
INSERT INTO comment VALUES (233, 58, 3, 'super top');
INSERT INTO comment VALUES (255, 33, 1, 'Une super aventure');
INSERT INTO comment VALUES (257, 33, 2, 'trop bien');
INSERT INTO comment VALUES (268, 33, 3, 'super top');
INSERT INTO comment VALUES (288, 19, 1, 'Une super aventure');
INSERT INTO comment VALUES (289, 19, 2, 'trop bien');
INSERT INTO comment VALUES (291, 19, 3, 'super top');
INSERT INTO comment VALUES (142, 13, 1, 'Une super aventure');
INSERT INTO comment VALUES (143, 13, 2, 'trop bien');
INSERT INTO comment VALUES (144, 13, 3, 'super top');
INSERT INTO comment VALUES (171, 14, 1, 'Une super aventure');
INSERT INTO comment VALUES (173, 14, 2, 'trop bien');
INSERT INTO comment VALUES (174, 14, 3, 'super top');
INSERT INTO comment VALUES (205, 47, 1, 'Une super aventure');
INSERT INTO comment VALUES (207, 47, 2, 'trop bien');
INSERT INTO comment VALUES (209, 47, 3, 'super top');
INSERT INTO comment VALUES (213, 66, 1, 'Une super aventure');
INSERT INTO comment VALUES (216, 66, 2, 'trop bien');
INSERT INTO comment VALUES (217, 66, 3, 'super top');
INSERT INTO comment VALUES (238, 72, 1, 'Une super aventure');
INSERT INTO comment VALUES (240, 72, 2, 'trop bien');
INSERT INTO comment VALUES (242, 72, 3, 'super top');
INSERT INTO comment VALUES (260, 84, 1, 'Une super aventure');
INSERT INTO comment VALUES (261, 84, 2, 'trop bien');
INSERT INTO comment VALUES (263, 84, 3, 'super top');
INSERT INTO comment VALUES (284, 56, 1, 'Une super aventure');
INSERT INTO comment VALUES (286, 56, 2, 'trop bien');
INSERT INTO comment VALUES (290, 56, 3, 'super top');
INSERT INTO comment VALUES (115, 90, 1, 'Une super aventure');
INSERT INTO comment VALUES (135, 90, 2, 'trop bien');
INSERT INTO comment VALUES (140, 90, 3, 'super top');
INSERT INTO comment VALUES (157, 36, 1, 'Une super aventure');
INSERT INTO comment VALUES (162, 36, 2, 'trop bien');
INSERT INTO comment VALUES (167, 36, 3, 'super top');
INSERT INTO comment VALUES (194, 30, 1, 'Une super aventure');
INSERT INTO comment VALUES (195, 30, 2, 'trop bien');
INSERT INTO comment VALUES (196, 30, 3, 'super top');
INSERT INTO comment VALUES (211, 16, 1, 'Une super aventure');
INSERT INTO comment VALUES (212, 16, 2, 'trop bien');
INSERT INTO comment VALUES (215, 16, 3, 'super top');
INSERT INTO comment VALUES (245, 31, 1, 'Une super aventure');
INSERT INTO comment VALUES (250, 31, 2, 'trop bien');
INSERT INTO comment VALUES (253, 31, 3, 'super top');
INSERT INTO comment VALUES (275, 52, 1, 'Une super aventure');
INSERT INTO comment VALUES (280, 52, 2, 'trop bien');
INSERT INTO comment VALUES (281, 52, 3, 'super top');


--
-- Name: comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: adventurer
--

SELECT pg_catalog.setval('comment_id_seq', 291, true);


--
-- PostgreSQL database dump complete
--

