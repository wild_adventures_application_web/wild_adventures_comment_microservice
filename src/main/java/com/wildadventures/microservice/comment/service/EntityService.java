package com.wildadventures.microservice.comment.service;

import java.util.List;

public interface EntityService<T> {
	
	T findOne(Long id);
		
	List<T> handleFind(Integer page, Integer size, Boolean filter, T entity);

	void insertMany(List<T> entities);

	void updateMany(List<T> entities);

	void deleteMany(List<T> entities);





}
