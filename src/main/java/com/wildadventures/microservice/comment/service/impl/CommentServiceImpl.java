package com.wildadventures.microservice.comment.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.wildadventures.microservice.comment.domain.Comment;
import com.wildadventures.microservice.comment.exception.BadRequestException;
import com.wildadventures.microservice.comment.exception.NotFoundException;
import com.wildadventures.microservice.comment.repository.CommentRepository;
import com.wildadventures.microservice.comment.service.EntityService;

@Service
public class CommentServiceImpl implements EntityService<Comment> {

	@Autowired
	private CommentRepository commentRepository;

	@Override
	public Comment findOne(Long commentId) {
		Optional<Comment> optComment = this.commentRepository.findById(commentId);
		String errorMsg = String.format("No comment with id %d.", commentId);

		return optComment.orElseThrow(() -> new NotFoundException(errorMsg));
	}

	@Override
	public List<Comment> handleFind(Integer page, Integer size, Boolean filter, Comment entity) {
		List<Comment> comments;

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		if (!filter && (page == null || size == null)) {
			comments = this.commentRepository.findAll();
		} else if (!filter && (page != null && size != null)) {
			comments = this.commentRepository.findAll(PageRequest.of(page, size)).getContent();
		} else if (filter && (page == null || size == null)) {
			comments = this.commentRepository.findAll(Example.of(entity, matcher));
		} else {
			comments = this.commentRepository.findAll(Example.of(entity, matcher), PageRequest.of(page, size))
					.getContent();
		}
		
		if (comments.isEmpty()) {
			throw new NotFoundException("No comments found.");
		}
		
		return comments;
	}

	@Override
	public void insertMany(List<Comment> comments) {
		String errorMsg;

		if (comments.isEmpty()) {
			errorMsg = "No comments provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Comment comment : comments) {
			if (comment.getId() != null) {
				throw new BadRequestException("The ids are auto-generated, hint: use PUT to update an entity.");
			}
		}

		this.commentRepository.saveAll(comments);
	}

	@Override
	public void updateMany(List<Comment> comments) {
		String errorMsg;

		if (comments.isEmpty()) {
			errorMsg = "No comments provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Comment comment : comments) {
			if (comment.getId() == null) {
				errorMsg = "An comment has no Id. Hint: use POST to insert an entity.";
				throw new BadRequestException(errorMsg);
			} else if (!this.commentRepository.existsById(comment.getId())) {
				errorMsg = String.format("No comment with id %d.", comment.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.commentRepository.saveAll(comments);
	}

	@Override
	public void deleteMany(List<Comment> comments) {
		String errorMsg;

		if (comments.isEmpty()) {
			errorMsg = "No comments provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Comment comment : comments) {
			if (comment.getId() == null) {
				errorMsg = "An comment has no Id. Cannot delete an comment if no id is provided.";
				throw new BadRequestException(errorMsg);
			} else if (!this.commentRepository.existsById(comment.getId())) {
				errorMsg = String.format("No comment with id %d.", comment.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.commentRepository.deleteInBatch(comments);
	}
}
