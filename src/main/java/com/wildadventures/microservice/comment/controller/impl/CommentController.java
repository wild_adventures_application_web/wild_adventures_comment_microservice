package com.wildadventures.microservice.comment.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wildadventures.microservice.comment.controller.EntityController;
import com.wildadventures.microservice.comment.domain.Comment;
import com.wildadventures.microservice.comment.service.EntityService;

@RestController
@RequestMapping(path = "/comments", produces = "application/json")
public class CommentController implements EntityController<Comment> {

	@Autowired
	private EntityService<Comment> commentService;

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<Comment> getOne(@PathVariable Long id) {
		Comment comment = this.commentService.findOne(id);

		return new ResponseEntity<Comment>(comment, HttpStatus.OK);
	}

	@GetMapping
	@Override
	public ResponseEntity<List<Comment>> getMany(
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false, defaultValue = "false") boolean filter,
			@RequestParam(required = false) Long itemId,
			@RequestParam(required = false) Long userId) {
		Comment comment = new Comment();
		comment.setItemId(itemId);
		comment.setUserId(userId);

		List<Comment> comments = this.commentService.handleFind(page, size, filter, comment);

		return new ResponseEntity<List<Comment>>(comments, HttpStatus.OK);
	}

	@PostMapping
	@Override
	public ResponseEntity<Void> postMany(@RequestBody List<Comment> comments) {
		this.commentService.insertMany(comments);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping
	@Override
	public ResponseEntity<Void> putMany(@RequestBody List<Comment> comments) {
		this.commentService.updateMany(comments);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping
	@Override
	public ResponseEntity<Void> deleteMany(@RequestBody List<Comment> comments) {
		this.commentService.deleteMany(comments);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
