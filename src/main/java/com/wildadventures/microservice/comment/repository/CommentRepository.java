package com.wildadventures.microservice.comment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wildadventures.microservice.comment.domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

}
